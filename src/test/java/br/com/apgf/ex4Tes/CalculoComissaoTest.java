
package br.com.apgf.ex4Tes;

import br.com.apgf.ex4.CalculoComissao;
import br.com.apgf.ex4.Comissao;
import org.junit.Test;
import static org.junit.Assert.*;

public class CalculoComissaoTest {
     
    @Test   
    public void deveMostraComissaoDoVendedor(){
        Comissao comissao = new Comissao("Daniel", "Camisa", 11.32, 10);
        CalculoComissao calculo = new CalculoComissao();
        Double resultado = calculo.Calcular(comissao);
        assertEquals(5.66, resultado, 0.0001);
        }    
}    

