
package br.com.apgf.ex4;

public class Comissao {
    
    private String Vendedor;
    private String Peca;
    private double precoUnitario;
    private double QuantidadeVendida;

    public Comissao() {
    }

    public Comissao(String Vendedor, String Peca, double precoUnitario, double QuantidadeVendida) {
        this.Vendedor = Vendedor;
        this.Peca = Peca;
        this.precoUnitario = precoUnitario;
        this.QuantidadeVendida = QuantidadeVendida;
    }

    
    
    public String getVendedor() {
        return Vendedor;
    }

    public void setVendedor(String Vendedor) {
        this.Vendedor = Vendedor;
    }

    public String getPeca() {
        return Peca;
    }

    public void setPeca(String Peca) {
        this.Peca = Peca;
    }

    public double getPrecoUnitario() {
        return precoUnitario;
    }

    public void setPrecoUnitario(double precoUnitario) {
        this.precoUnitario = precoUnitario;
    }

    public double getQuantidadeVendida() {
        return QuantidadeVendida;
    }

    public void setQuantidadeVendida(double QuantidadeVendida) {
        this.QuantidadeVendida = QuantidadeVendida;
    } 
    
    public double TotalVenda(){
        return this.QuantidadeVendida * this.precoUnitario;
    }
    
}
